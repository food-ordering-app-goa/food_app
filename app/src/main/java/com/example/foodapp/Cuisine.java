package com.example.foodapp;

public enum Cuisine {
    AMERICAN("American"),
    CHINESE("Chinese"),
    VIETNAMESE("Vietnamese"),
    JAPANESE("Japanese"),
    MEXICAN("Mexican"),
    VEGAN("Vegan");

    Cuisine(String cuisineName) { this.cuisineName = cuisineName; }

    private final String cuisineName;

    @Override
    public String toString() {
        return cuisineName;
    }
}

