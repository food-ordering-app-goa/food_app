package com.example.foodapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        // Starts Login Activity after three seconds
        new Handler().postDelayed(() -> {
             Intent main = new Intent(SplashScreen.this, LoginActivity.class);
             startActivity(main);
             finish();
        }, 3000);
    }
}