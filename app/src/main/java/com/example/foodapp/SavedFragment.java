package com.example.foodapp;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SavedFragment extends Fragment {

    View rootView;

    public static ArrayList<Dish> savedDishes = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_saved, container, false);

        initRecyclerView();

        return rootView;
    }

    /**
     * Sets up the Recycler View and attaches a Gridlayout manager with two columns
     */
    private void initRecyclerView() {
        RecyclerView recyclerView = rootView.findViewById(R.id.saved_recycler_view);
        recyclerView.setHasFixedSize(true);
        SavedAdapter savedAdapter = new SavedAdapter(getContext(), savedDishes);
        recyclerView.setAdapter(savedAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }

    private static class SavedAdapter extends RecyclerView.Adapter<SavedAdapter.SavedViewHolder> {

        private Context mContext;
        private ArrayList<Dish> mDishes;

        public SavedAdapter(Context mContext, ArrayList<Dish> mDishes) {
            this.mContext = mContext;
            this.mDishes = mDishes;
        }

        @NonNull
        @Override
        public SavedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.saved_items, parent, false);
            return new SavedViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SavedViewHolder holder, int position) {
            holder.dish_name.setText(mDishes.get(position).getName());
            Glide.with(mContext).asBitmap().load(mDishes.get(position).getDrawableId()).into(holder.dish_art);
            DecimalFormat decimalFormat = new DecimalFormat("###.##");
            String formattedPrice = "$" + decimalFormat.format(mDishes.get(position).getPrice());
            holder.dish_price.setText(formattedPrice);
            holder.dish_saved.setColorFilter(ContextCompat.getColor(mContext, R.color.light_grey), android.graphics.PorterDuff.Mode.SRC_IN);

            holder.itemView.setOnClickListener(v -> {
                DishDialogPopUp dishDialogPopUp = new DishDialogPopUp(mContext, position, mDishes);
                dishDialogPopUp.initDialog();
            });
        }

        @Override
        public int getItemCount() {
            return mDishes.size();
        }

        private class SavedViewHolder extends RecyclerView.ViewHolder {
            TextView dish_name, dish_price;
            ImageView dish_art, dish_saved;
            public SavedViewHolder(@NonNull View itemView) {
                super(itemView);
                dish_name = itemView.findViewById(R.id.dish_name_name);
                dish_art = itemView.findViewById(R.id.dish_picture);
                dish_price = itemView.findViewById(R.id.price);
                dish_saved = itemView.findViewById(R.id.saved_indicator);
            }
        }
    }
}
