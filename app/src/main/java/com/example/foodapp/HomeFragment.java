package com.example.foodapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private RecyclerView recyclerView;
    private CuisineAdapter cuisineAdapter;
    private View rootView;


    private ArrayList<Integer> cuisineImage = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        initHomeArrays();
        initRecyclerView();

        return rootView;
    }

    private void initHomeArrays() {

        // Adds Image Resources to the Array
        cuisineImage.add(R.drawable.american_pic);
        cuisineImage.add(R.drawable.chinese_pic);
        cuisineImage.add(R.drawable.vietnamese_pics);
        cuisineImage.add(R.drawable.japanese_pics);
        cuisineImage.add(R.drawable.mexican_pics);
        cuisineImage.add(R.drawable.vegan_pic);

    }

    private void initRecyclerView() {
        recyclerView = rootView.findViewById(R.id.cuisine_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(true);
        cuisineAdapter = new CuisineAdapter(getContext(), LoginActivity.cuisines);
        recyclerView.setAdapter(cuisineAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,
                false));
    }

    private class CuisineAdapter extends RecyclerView.Adapter<CuisineAdapter.CuisineViewHolder> {

        private Context mContext;
        private ArrayList<ArrayList<Restaurant>> mCuisine;

        public CuisineAdapter(Context mContext, ArrayList<ArrayList<Restaurant>> mCuisines) {
            this.mContext = mContext;
            this.mCuisine = mCuisines;
        }

        @NonNull
        @Override
        public CuisineAdapter.CuisineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.cuisine_items, parent, false);
            return new CuisineViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CuisineAdapter.CuisineViewHolder holder, int position) {
            holder.cuisine_name.setText(mCuisine.get(position).get(0).getCategory().toString());
            int[] cuisineColors = getContext().getResources().getIntArray(R.array.cuisineColors);
            holder.itemView.setBackgroundColor(cuisineColors[position]);
            if (position == 1 || position == 4) {
                holder.cuisine_name.setTextColor(getContext().getResources().getColor(R.color.black, null));
            }
            Glide.with(mContext).asBitmap().load(cuisineImage.get(position)).into(holder.cuisine_art);
            holder.itemView.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                RestaurantFragment restaurantFragment = new RestaurantFragment();
                restaurantFragment.setArguments(bundle);
                ((MainActivity) getActivity()).replaceFragments(restaurantFragment);
            });
        }

        @Override
        public int getItemCount() {
            return mCuisine.size();
        }

        private class CuisineViewHolder extends RecyclerView.ViewHolder {
            TextView cuisine_name;
            ImageView cuisine_art;
            public CuisineViewHolder(@NonNull View itemView) {
                super(itemView);
                cuisine_name = itemView.findViewById(R.id.cuisine_text);
                cuisine_art = itemView.findViewById(R.id.cuisine_food_pic);
            }
        }
    }
}
