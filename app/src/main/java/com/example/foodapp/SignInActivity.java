package com.example.foodapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SignInActivity extends AppCompatActivity {

    private Button signIn;
    private EditText email, password, username, preferredAddress;

    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        initViews();

        signIn.setOnClickListener(v -> signInUser());
    }

    /**
     * Assigns the ids to the view variable
     */
    private void initViews() {
        email = findViewById(R.id.emailLog);
        password = findViewById(R.id.passwordLog);
        username = findViewById(R.id.usernameLog);
        preferredAddress = findViewById(R.id.preferredAddressLog);
        signIn = findViewById(R.id.signUpButton);
    }

    /**
     * Validates email, password, and username, and if all checks out, goes to isUser()
     */
    private void signInUser() {
        if (!validateEmail() | !validatePassword() | !validateUsername()) {
            return;
        } else {
            isUser();
        }
    }

    private void isUser() {
        reference = FirebaseDatabase.getInstance().getReference("users");

        String userEnteredEmail = email.getText().toString().trim();
        String userEnteredPassword = password.getText().toString().trim();
        String userEnteredUsername = username.getText().toString().trim();
        String userEnteredPreferredAddress = preferredAddress.getText().toString().trim();
        String userSavedDishes = "";

        UserProfile userProfile = new UserProfile(userEnteredEmail, userEnteredPassword, userEnteredUsername, userEnteredPreferredAddress, userSavedDishes);

        reference.child(userEnteredUsername).setValue(userProfile);

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);

        intent.putExtra("username", userEnteredUsername);

        MainActivity.fromLogin = true;

        startActivity(intent);
    }

    /**
     * @return whether Username is valid
     */
    private boolean validateUsername() {
        String val = username.getText().toString();

        if (val.isEmpty()) {
            username.setError("Field cannot be empty");
            return false;
        } else if (val.contains(" ")) {
            username.setError("Username cannot have spaces");
            return false;
        } else if (val.length() >= 12) {
            username.setError("Username is too long");
            return false;
        } else {
            username.setError(null);
            return true;
        }
    }

    /**
     * @return whether password is valid
     */
    private boolean validatePassword() {
        String val = password.getText().toString();
        String passwordVal = "^" +
                "(?=.*[0-9])" +         // Must include a digit
                "(?=.*[a-zA-Z])" +      // Must include letters
                "(?=\\S+$)" +           // No white space
                ".{4,}" +               // Must be at least four characters
                "$";

        if (val.isEmpty()) {
            password.setError("Field cannot be empty");
            return false;
        } else if (!val.matches(passwordVal)) {
            password.setError("Password is too weak");
            return false;
        } else {
            password.setError(null);
            return true;
        }
    }

    /**
     * @return whether email is valid
     */
    private boolean validateEmail() {
        String val = email.getText().toString();

        if (val.isEmpty()) {
            email.setError("Field cannot be empty");
            return false;
        } else {
            email.setError(null);
            return true;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////
}