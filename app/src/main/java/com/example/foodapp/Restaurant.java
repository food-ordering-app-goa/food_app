package com.example.foodapp;

import java.util.ArrayList;

public class Restaurant {
    ArrayList<Dish> dishes;
    Cuisine category;
    String name;
    double rating;
    String address;
    String phoneNo;

    public Restaurant(ArrayList<Dish> dishes, Cuisine category,String name, double rating, String address, String phoneNo) {
        this.dishes = dishes;
        this.category = category;
        this.name = name;
        this.rating = rating;
        this.address = address;
        this.phoneNo = phoneNo;
    }

    public ArrayList<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(ArrayList<Dish> dishes) {
        this.dishes = dishes;
    }

    public Cuisine getCategory() {
        return category;
    }

    public void setCategory(Cuisine category) {
        this.category = category;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}
