package com.example.foodapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RestaurantFragment extends Fragment {

    RecyclerView recyclerView;
    RestaurantAdapter restaurantAdapter;
    View rootView;
    ImageButton back;
    TextView cuisine;

    private int posRestaurant;
    ArrayList<Restaurant> currentCuisine = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_restaurant, container, false);

        initViews();

        back.setOnClickListener(v -> {
            HomeFragment homeFragment = new HomeFragment();
            MainActivity.fragmentPos = 0;
            MainActivity.fromLogin = false;
            ((MainActivity) getActivity()).replaceFragments(homeFragment);
        });

        initRestaurantActivity();
        initRecyclerView();

        return rootView;
    }

    private void initViews() {
        back = rootView.findViewById(R.id.back_button_cuisine_page);
        cuisine = rootView.findViewById(R.id.cuisine_text_in_restaurant);
    }

    private void initRestaurantActivity() {
        posRestaurant = getArguments().getInt("position", 0);
        currentCuisine = LoginActivity.cuisines.get(posRestaurant);
        cuisine.setText(LoginActivity.cuisines.get(posRestaurant).get(0).getCategory().toString());
    }

    private void initRecyclerView() {
        recyclerView = rootView.findViewById(R.id.restaurant_recycler_view);
        recyclerView.setHasFixedSize(true);
        restaurantAdapter = new RestaurantAdapter(getContext(), currentCuisine);
        recyclerView.setAdapter(restaurantAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,
                false));
    }

    private class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolder> {

        private Context mContext;
        private ArrayList<Restaurant> mRestaurant;

        public RestaurantAdapter(Context mContext, ArrayList<Restaurant> mRestaurant) {
            this.mContext = mContext;
            this.mRestaurant = mRestaurant;
        }

        @NonNull
        @Override
        public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.restaurant_items, parent, false);
            return new RestaurantViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
            holder.restaurant_name.setText(mRestaurant.get(position).getName());
            Glide.with(mContext).asBitmap().load(mRestaurant.get(position).getDishes().get(0).drawableId).into(holder.restaurant_art);
            holder.restaurant_cuisine.setText(mRestaurant.get(position).getCategory().toString());
            holder.restaurant_address.setText(mRestaurant.get(position).getAddress());
            holder.restaurant_phone.setText(mRestaurant.get(position).getPhoneNo());
            holder.restaurant_description.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut lacinia tortor.");
            holder.itemView.setOnClickListener(v -> {
                DishesFragment dishesFragment = new DishesFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                bundle.putInt("prevPosition", posRestaurant);
                dishesFragment.setArguments(bundle);
                ((MainActivity) getActivity()).replaceFragments(dishesFragment);
            });
        }

        @Override
        public int getItemCount() {
            return mRestaurant.size();
        }

        private class RestaurantViewHolder extends RecyclerView.ViewHolder {
            TextView restaurant_name, restaurant_description, restaurant_cuisine, restaurant_address, restaurant_phone;
            ImageView restaurant_art;
            public RestaurantViewHolder(@NonNull View itemView) {
                super(itemView);
                restaurant_name = itemView.findViewById(R.id.restaurant_name_name);
                restaurant_art = itemView.findViewById(R.id.restaurant_picture);
                restaurant_description = itemView.findViewById(R.id.cuisine_restaurant_description);
                restaurant_cuisine = itemView.findViewById(R.id.cuisine_restaurant_lol);
                restaurant_address = itemView.findViewById(R.id.address_lol);
                restaurant_phone = itemView.findViewById(R.id.phone_number_lol);
            }
        }
    }
}
