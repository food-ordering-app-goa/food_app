package com.example.foodapp;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    GoogleMap mMap;
    SearchView searchView;
    Button checkout;
    TextView progress, exit;
    ProgressBar progressBar;
    CardView anotherOrder;

    LatLng latLng;

    private int i = 0;
    private int deliveryTime = 0;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        // Init search view
        searchView = findViewById(R.id.sv_location);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                moveMarker(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        // Init checkout
        checkout = findViewById(R.id.checkout);
        checkout.setOnClickListener(v -> {
            initCheckoutDialog();
        });

        mapFragment.getMapAsync(this);
    }

    /**
     * Initializes the checkout dialog and starts the progress bar
     */
    private void initCheckoutDialog() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.checkoutpopup);

        // In minutes
        deliveryTime = 2;

        progress = dialog.findViewById(R.id.checkout_progress);
        progressBar = dialog.findViewById(R.id.checkout_progress_bar);
        anotherOrder = dialog.findViewById(R.id.checkout_another_order);
        exit = dialog.findViewById(R.id.checkout_exit);

        exit.setOnClickListener(v -> dialog.dismiss());

        anotherOrder.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainActivity.class);
            MainActivity.fragmentPos = 0;
            startActivity(intent);
        });

        startProgressBar(deliveryTime);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    /**
     * Progresses the progress bar
     */
    private void startProgressBar(int deliveryTime) {
        i = progressBar.getProgress();
        new Thread(() -> {
            while (i < 100) {
                i += 1;
                // Update the progress bar and display the current value in text view
                handler.post(() -> {
                    progressBar.setProgress(i);
                    String conProgress = i+"%";
                    progress.setText(conProgress);
                });
                try {
                    // Sleep for 100 milliseconds to show the progress slowly.
                    int deliverTimeMilliseconds = deliveryTime * 60 * 10;
                    Thread.sleep(deliverTimeMilliseconds);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Adds marker in preferred location and moves the camera
        Log.e("   ", " " + MainActivity.address);
        moveMarker(MainActivity.address);
    }

    /**
     * Moves marker to location
     * @param location the String address to move marker to
     */
    private void moveMarker(String location) {
        List<Address> addressList = null;
        if (location != null || !location.equals("")) {
            Geocoder geocoder = new Geocoder(MapsActivity.this);
            try {
                addressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException ignored) {

            }
            Address address = addressList.get(0);
            latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.addMarker(new MarkerOptions().position(latLng).title(location));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
        }
    }
}