package com.example.foodapp;

public class Dish {
    String name;
    String restaurant;
    double rating;
    double price;
    int drawableId;

    public Dish(String name, String restaurant, double rating, double price, int drawableId) {
        this.name = name;
        this.restaurant = restaurant;
        this.rating = rating;
        this.price = price;
        this.drawableId = drawableId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }
}
