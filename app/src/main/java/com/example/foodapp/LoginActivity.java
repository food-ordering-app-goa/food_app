package com.example.foodapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private Button loginButton, signUpButton;
    private EditText username, password;
    private CheckBox rememberMe;

    public static ArrayList<Restaurant> restaurants = new ArrayList<>();
    public static ArrayList<ArrayList<Restaurant>> cuisines = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews(); // Assign the id (R.id.emailLog) in this method.
        initRestaurants(); // Creates the dishes and assigns them to restaurants.

        loginButton.setOnClickListener(v -> {
            logInUser(); // Checks entered information with database.
        });

        signUpButton.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
            startActivity(intent);
        });

        // For remember me button
        SharedPreferences preferences = getSharedPreferences("checkbox", MODE_PRIVATE);
        String checkbox = preferences.getString("remember", "");
        String editorUsername = preferences.getString("username", "");
        if (checkbox.equals("true")) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("username", editorUsername);
            MainActivity.fromLogin = true;
            startActivity(intent);
        }

        // For remember me button
        rememberMe.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (username.getText().toString().equals("") && password.getText().toString().equals("")) {
                rememberMe.setChecked(false);
                Toast.makeText(LoginActivity.this, "Please fill in all required fields", Toast.LENGTH_SHORT).show();
            } else {
                if (buttonView.isChecked()) {

                    SharedPreferences preferences1 = getSharedPreferences("checkbox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences1.edit();
                    editor.putString("remember", "true");
                    editor.putString("username", username.getText().toString());
                    editor.apply();


                } else if (!buttonView.isChecked()) {

                    SharedPreferences preferences1 = getSharedPreferences("checkbox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences1.edit();
                    editor.putString("remember", "false");
                    editor.apply();


                }
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////////////

    /**
     * Assigns the id values to the view variables
     */
    private void initViews() {
        username = findViewById(R.id.emailLog);
        password = findViewById(R.id.passwordLog);
        loginButton = findViewById(R.id.loginButton);
        signUpButton = findViewById(R.id.signUpButton);
        rememberMe = findViewById(R.id.stayLoggedIn);
    }

    ///////////////////////////////////////////////////////////////////////////////////

    /**
     * Checks that username and password field is not empty, then proceeds to check Firebase
     */
    private void logInUser() {
        if (!validateUsername() | !validatePassword()) {
            return;
        } else {
            isUser();
        }
    }

    /**
     * Compares user entered things with Firebase database, and only allows if it all checks out.
     */
    private void isUser() {
        String userEnteredUsername = username.getText().toString().trim();
        String userEnteredPassword = password.getText().toString().trim();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("users");

        Query checkUser = reference.orderByChild("username").equalTo(userEnteredUsername);

        checkUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {

                    username.setError(null);

                    String passwordFromDB = snapshot.child(userEnteredUsername).child("password").getValue(String.class);

                    if (passwordFromDB.equals(userEnteredPassword)) {
                        username.setError(null);

                        String usernameFromDB = snapshot.child(userEnteredUsername).child("username").getValue(String.class);

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                        intent.putExtra("username", usernameFromDB);

                        MainActivity.fromLogin = true;
                        startActivity(intent);
                    } else {
                        password.setError("Wrong Password");
                        password.requestFocus();
                    }
                } else {
                    username.setError("That Email Does Not Exist");
                    username.requestFocus();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    /**
     * @return true if password field is not empty
     */
    private boolean validatePassword() {
        String val = password.getText().toString();

        if (val.isEmpty()) {
            password.setError("Field cannot be empty");
            return false;
        } else {
            password.setError(null);
            return true;
        }
    }

    /**
     * @return true if username field is not empty
     */
    private boolean validateUsername() {
        String val = username.getText().toString();

        if (val.isEmpty()) {
            username.setError("Field cannot be empty");
            return false;
        } else {
            username.setError(null);
            return true;
        }

    }

    ///////////////////////////////////////////////////////////////////////////////////

    /**
     * Basically initializes the data base by adding the dishes to their respective restaurants
     */
    private void initRestaurants() {

        // Sets up the ArrayLists of the cuisines
        ArrayList<Restaurant> american = new ArrayList<>();
        ArrayList<Restaurant> chinese = new ArrayList<>();
        ArrayList<Restaurant> vietnamese = new ArrayList<>();
        ArrayList<Restaurant> japanese = new ArrayList<>();
        ArrayList<Restaurant> mexican = new ArrayList<>();
        ArrayList<Restaurant> vegan = new ArrayList<>();

        ArrayList<Dish> restaurantDishes;

        restaurantDishes = initCactiClubCafe(); // Adds dishes in method initCactiClub() to an ArrayList
        Restaurant cactiClubCafe = new Restaurant(restaurantDishes, Cuisine.AMERICAN,
                "Cacti Club Cafe", 0.0, "8253 Whitemarsh St. Potomac, MD 20854", "(340) 727-9392"); // Creates an instance of Restaurant with the dish ArrayList created one line before.
        restaurants.add(cactiClubCafe); // Adds the Restaurant instance to a ArrayList

        restaurantDishes = initBostoonPizzia();
        Restaurant bostoonPizzia = new Restaurant(restaurantDishes, Cuisine.AMERICAN, "Bostoon Pizzia",0.0, "2 Ennersdale Close, Coleshill, B46 1HA", "(494) 579-7404");
        restaurants.add(bostoonPizzia);

        restaurantDishes = initNewLorkFries();
        Restaurant newLorkFries = new Restaurant(restaurantDishes, Cuisine.AMERICAN, "New Lork Fries",0.0, "4 Luker Drive, Petersfield, GU31 4SN", "(990) 980-4550");
        restaurants.add(newLorkFries);

        restaurantDishes = initBoilingLimit();
        Restaurant boilingLimit = new Restaurant(restaurantDishes, Cuisine.CHINESE, "Boiling Limit",0.0, "30 Beach Road, Emsworth, PO10 7HR", "(707) 565-2607");
        restaurants.add(boilingLimit);

        restaurantDishes = initPearlFortressCafe();
        Restaurant pearlFortressCafe = new Restaurant(restaurantDishes, Cuisine.CHINESE, "Pearl Cafe", 0.0, "77 Cactus Drive Yuba City, CA 95993", "(278) 418-5020");
        restaurants.add(pearlFortressCafe);

        restaurantDishes = initDynastyDumplingFortress();
        Restaurant dynastyDumplingFortress = new Restaurant(restaurantDishes, Cuisine.CHINESE, "Dumpling Fortress",0.0, "70 Chandlers Drive, Erith, DA8 1LN", "(374) 318-6730");
        restaurants.add(dynastyDumplingFortress);

        restaurantDishes = initMuioGarden();
        Restaurant muioGarden = new Restaurant(restaurantDishes, Cuisine.CHINESE, "Muio Garden",0.0, "21 Drury Lane, Melbourn, SG8 6EP", "(967) 394-7173");
        restaurants.add(muioGarden);

        restaurantDishes = initBanhMiUnoBon();
        Restaurant banhMiUnoBon = new Restaurant(restaurantDishes, Cuisine.VIETNAMESE, "Mi Uno Bon",0.0, "10 The Heights, Danbury, CM3 4AG", "(690) 324-6886");
        restaurants.add(banhMiUnoBon);

        restaurantDishes = initTokyoBobSushi();
        Restaurant tokyoBobSushi = new Restaurant(restaurantDishes, Cuisine.JAPANESE, "Tokyo Bob Sushi",0.0, "103 Sefton Road, Litherland, L21 9HF", "(250) 778-4358");
        restaurants.add(tokyoBobSushi);

        restaurantDishes = initTakayayaSushi();
        Restaurant takayayaSushi = new Restaurant(restaurantDishes, Cuisine.JAPANESE, "Takayaya Sushi",0.0, "1C Olive Grove, London, N15 3BJ", "(884) 847-8716");
        restaurants.add(takayayaSushi);

        restaurantDishes = initKatsuSaan();
        Restaurant katsuSaan = new Restaurant(restaurantDishes, Cuisine.JAPANESE, "Katsu Saan",0.0, "7 Sandray Grove, Salford, M5 5QY", "(878) 762-6723");
        restaurants.add(katsuSaan);

        restaurantDishes = initChippotle();
        Restaurant chippotle = new Restaurant(restaurantDishes, Cuisine.MEXICAN, "Chippotle",0.0, "113 Irving Road, Bournemouth, BH6 5BJ", "(386) 633-5483");
        restaurants.add(chippotle);

        restaurantDishes = initTacoDeloMaro();
        Restaurant tacoDeloMaro = new Restaurant(restaurantDishes, Cuisine.MEXICAN, "Taco Delo Maro",0.0, "12 The Avenue, Crowthorne, RG45 6PD", "(244) 858-6256");
        restaurants.add(tacoDeloMaro);

        restaurantDishes = initChickpod();
        Restaurant chickpod = new Restaurant(restaurantDishes, Cuisine.VEGAN, "Chickpod",0.0, "1C Colenzo Drive, Andover, SP10 1JS", "(539) 234-0650");
        restaurants.add(chickpod);

        for (Restaurant restaurant : restaurants) {
            Cuisine cuisine = restaurant.getCategory();
            switch (cuisine) {
                case AMERICAN:
                    american.add(restaurant);
                    break;
                case CHINESE:
                    chinese.add(restaurant);
                    break;
                case VIETNAMESE:
                    vietnamese.add(restaurant);
                    break;
                case JAPANESE:
                    japanese.add(restaurant);
                    break;
                case MEXICAN:
                    mexican.add(restaurant);
                    break;
                case VEGAN:
                    vegan.add(restaurant);
                    break;
            }
        }

        cuisines.add(american);
        cuisines.add(chinese);
        cuisines.add(vietnamese);
        cuisines.add(japanese);
        cuisines.add(mexican);
        cuisines.add(vegan);
    }

    private ArrayList<Dish> initChickpod() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Falafel Platter", "Shawarma Platter",
                "Latkes Benechick", "Shakshuka Platter",
                "Shnizelonim Platter"};
        String restaurant = "Chickpod";
        /*rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("dishes");
        for (int i = 0; i < names.length; i++) {
            RatingsHelper ratingsHelper = new RatingsHelper(names[i], 0.0);
            reference.child(names[i]).setValue(ratingsHelper);
        }*/
        double[] price = {18.75, 18.75, 18.00, 18.75, 19.75};
        int[] drawableId = {R.drawable.falafelplatter, R.drawable.shawarmaplatter,
                R.drawable.latkes, R.drawable.shakshukaplatter,
                R.drawable.shnizelonimplatter};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initNewLorkFries() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Classic Poutine", "Bacon Double Cheese Poutine",
                "Butter Chicken Poutine", "Veggie Fries",
                "Pulled Pork Poutine"};
        String restaurant = "New Lork Fries";
        double[] price = {7.19, 7.19, 8.19, 7.19, 9.19};
        int[] drawableId = {R.drawable.classicpoutine, R.drawable.bacondoublecheesepoutine,
                R.drawable.butterchickenpoutine, R.drawable.veggiefries,
                R.drawable.pulledpokrpoutine};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initKatsuSaan() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Tonkatsu Set", "House Made Japanese Curry",
                "Ebi Hotate Katsu Set", "Ra-Yu Champon Ramen",
                "Chicken Katsu Set"};
        String restaurant = "Katsu Saan";
        double[] price = {15.00, 6.00, 17.00, 13.50, 16.00};
        int[] drawableId = {R.drawable.tonkatsuset, R.drawable.housemadejapanesecurry,
                R.drawable.ebihotatekatsuset, R.drawable.rayuchamponramen,
                R.drawable.chickenkatsuset};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initBoilingLimit() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Beef Hot Soup", "Milk Cream Curry Hot Soup",
                "Lamb Hot Soup", "House Special Hot Soup",
                "Spicy Tangy Beef"};
        String restaurant = "Boiling Limit";
        double[] price = {18.10, 17.88, 18.32, 18.32, 7.32};
        int[] drawableId = {R.drawable.beefhotsoup, R.drawable.milkcreamcurryhot,
                R.drawable.lambhotsoup, R.drawable.housespecialhotsoup,
                R.drawable.spicytangybeef};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initBostoonPizzia() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Burger Pizza", "Amigo Pizza",
                "Smoky Mountain Spaghetti & Meatballs", "Bacon Double Cheeseburger Pizza",
                "Slow-Roasted Pork Back Ribs Full Rack"};
        String restaurant = "Bostoon Pizzia";
        double[] price = {16.99, 14.29, 21.49, 14.79, 28.49};
        int[] drawableId = {R.drawable.burgerpizza, R.drawable.amigopizza,
                R.drawable.smokymountainspaghetti, R.drawable.bacondoublecheeseburger,
                R.drawable.slowroastedporkback};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initTacoDeloMaro() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Baja Burrito", "Baja Bowl",
                "Enchilada Platters", "Quesadilla", "Loaded Nachos"};
        String restaurant = "Taco Delo Maro";
        double[] price = {8.00, 8.80, 10.10, 8.80, 10.40};
        int[] drawableId = {R.drawable.bajaburrito, R.drawable.bajabowl,
                R.drawable.enchiladaplatters, R.drawable.quesadilla,
                R.drawable.loadednachos};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initTakayayaSushi() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Pork Katsu Curry", "Karaage & Curry",
                "Katsu Bento", "Teriyaki Bento", "Chicken Teriyaki Don"};
        String restaurant = "Takayaya Sushi";
        double[] price = {14.75, 13.75, 15.45, 15.95, 10.95};
        int[] drawableId = {R.drawable.porkkatsucurry, R.drawable.karaageandcurry,
                R.drawable.katsubento, R.drawable.teriyakibento,
                R.drawable.chickenteriyakidon};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initMuioGarden() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Pan-Fried Sliced Beef Rice Noodle", "Crispy Bun with Condensed Milk",
                "Sweet and Sour Pork", "Shrimp and Scrambled Egg", "Lobster Tail Laksa Noodle in Soup"};
        String restaurant = "Muio Garden";
        double[] price = {16.25, 5.00, 20.00, 22.50, 17.50};
        int[] drawableId = {R.drawable.beefstirfry, R.drawable.crispybunwithcondesnsed,
                R.drawable.sweetandsourpork, R.drawable.shrimpandscrambled,
                R.drawable.lobstertaillaksa};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initChippotle() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Burrito Bowl", "Chips & Queso Blanco",
                "Three Tacos", "Burrito", "Large Chips & Large Guacamole"};
        String restaurant = "Chippotle";
        double[] price = {10.60, 4.45, 10.60, 10.60, 7.25};
        int[] drawableId = {R.drawable.burritobowl, R.drawable.quesoblanco,
                R.drawable.threetacos, R.drawable.burrito,
                R.drawable.chipsandguacamole};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initDynastyDumplingFortress() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Pork Soup Dumpling (8 pcs)", "Pork Pot Sticker (8 pcs)",
                "Spicy Beef Noodle Soup", "Spicy String Beans", "Shanghai Fried Noodles"};
        String restaurant = "Dumpling Fortress";
        double[] price = {5.95, 5.25, 7.67, 11.17, 9.77};
        int[] drawableId = {R.drawable.porksoupdumpling, R.drawable.porkpotsticker,
                R.drawable.spicybeefnoodlesoup, R.drawable.spicystringbeans,
                R.drawable.shanghaifriednoodles};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initTokyoBobSushi() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"California Roll (8 pcs)", "Dynamite Roll (5 pcs)",
                "Salmon & Avocado Roll (8 pcs)", "Spicy Salmon Roll (8 pcs)", "Chopped Scallop Roll (8 pcs)"};
        String restaurant = "Tokyo Bob Sushi";
        double[] price = {5.70, 6.90, 6.60, 7.15, 6.60};
        int[] drawableId = {R.drawable.californiaroll, R.drawable.dynamiteroll,
                R.drawable.salmonandavocado, R.drawable.spicysalmonroll,
                R.drawable.choppedscalloproll};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initBanhMiUnoBon() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Beef Pho", "Grilled Lemongrass Chicken on Rice",
                "Banh Mi Poulet", "Chicken Pho", "Grilled Lemongrass and Chicken Spring Rolls"};
        String restaurant = "Mi Uno Bon";
        double[] price = {17.00, 18.00, 12.00, 16.00, 16.00};
        int[] drawableId = {R.drawable.beefpho, R.drawable.grilledlemongrasschickenwithrice,
                R.drawable.banhmipoulet, R.drawable.chickenpho,
                R.drawable.grilledlemongrassandchickenspring};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initPearlFortressCafe() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Fried Chicken Nuggets", "Chicken Teriyaki Fried Rice",
                "Three Cups Chicken", "House Special Beef Noodle Soup", "Taiwanese Pork Meat Sauce on Rice"};
        String restaurant = "Pearl Cafe";
        double[] price = {9.95, 17.25, 13.95, 15.50, 9.95};
        int[] drawableId = {R.drawable.chickennuggets, R.drawable.chickenteriyakifried,
                R.drawable.threecupschicken, R.drawable.housespecialbeef,
                R.drawable.taiwaneseporkmeat};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }

    private ArrayList<Dish> initCactiClubCafe() {
        ArrayList<Dish> dishes = new ArrayList<>();
        String[] names = {"Blackened Creole Chicken", "Chicken Wings", "Cheddar Bacon Burger",
                "Spicy Chicken", "Szechuan Chicken Lettuce Wraps", "Truffle Fries"};
        String restaurant = "Cacti Club Cafe";
        double[] price = {26.50, 15.75, 18.50, 15.75, 19.00, 10.50};
        int[] drawableId = {R.drawable.creolechicken, R.drawable.chickenwings,
                R.drawable.cheddarbaconburger, R.drawable.spicychicken,
                R.drawable.szechuanchicken, R.drawable.trufflefries};
        for (int i = 0; i < names.length; i++) {
            Dish dish = new Dish(names[i], restaurant, 0, price[i], drawableId[i]);
            dishes.add(dish);
        }
        return dishes;
    }
}