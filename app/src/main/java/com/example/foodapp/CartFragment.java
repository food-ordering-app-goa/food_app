package com.example.foodapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CartFragment extends Fragment {

    TextView grand_total, delivery_fee, total_number_of_items, username_cart, next;
    View rootView;

    private int totalQuantity;
    private double total;

    public static ArrayList<CartHelper> cartItems = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_cart, container, false);

        initViews();
        initRecyclerView();
        initCartFragment();

        return rootView;
    }

    /**
     * Initializes the views
     */
    private void initViews() {
        grand_total = rootView.findViewById(R.id.cart_total);
        delivery_fee = rootView.findViewById(R.id.cart_delivery);
        total_number_of_items = rootView.findViewById(R.id.cart_total_of);
        username_cart = rootView.findViewById(R.id.cart_username_shopping_cart);
        next = rootView.findViewById(R.id.cart_next_button);
    }

    /**
     * Assigns username to cart and sets an onClickListener to the next button
     */
    private void initCartFragment() {
        String username_concat = MainActivity.username + "'s Cart";
        username_cart.setText(username_concat);

        next.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MapsActivity.class);
            startActivity(intent);
        });
    }

    /**
     * Initializes the Recycler View and adds a Linear Layout Manager
     */
    private void initRecyclerView() {
        RecyclerView recyclerView = rootView.findViewById(R.id.cart_recycler_view);
        recyclerView.setHasFixedSize(true);
        CartAdapter cartAdapter = new CartAdapter(getContext(), cartItems);
        recyclerView.setAdapter(cartAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,
                false));
    }

    private class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

        private Context mContext;
        private ArrayList<CartHelper> mDishes;

        public CartAdapter(Context mContext, ArrayList<CartHelper> mDishes) {
            this.mContext = mContext;
            this.mDishes = mDishes;
        }

        @NonNull
        @Override
        public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.cart_items, parent, false);
            return new CartViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
            holder.dish_name.setText(mDishes.get(position).getDish().getName());
            Glide.with(mContext).asBitmap().load(mDishes.get(position).getDish().getDrawableId()).into(holder.dish_art);
            DecimalFormat decimalFormat = new DecimalFormat("###.00");
            String formattedPrice = "$" + decimalFormat.format(mDishes.get(position).getDish().getPrice());
            holder.dish_price.setText(formattedPrice);
            String formattedQuantity = Integer.toString(mDishes.get(position).getQuantity());
            holder.dish_quantity.setText(formattedQuantity);

            Log.e(" CartList  ", " " + cartItems);

            // Quantity in "Total <Quantity> items"
            totalQuantity += mDishes.get(position).getQuantity();
            String total_number_concat = "Total " + totalQuantity + " items";
            total_number_of_items.setText(total_number_concat);

            // Add total
            total += (mDishes.get(position).getDish().getPrice() * mDishes.get(position).getQuantity());
            String formattedTotal = "$" + decimalFormat.format(total);
            grand_total.setText(formattedTotal);

            // Increases the quantity, price, and total items and also changes the object data
            holder.dish_increase.setOnClickListener(v -> {
                // Quantity data in cart item
                int set = cartItems.get(position).getQuantity();
                set++;
                cartItems.get(position).setQuantity(set);

                // Quantity and price
                String quantity = Integer.toString(mDishes.get(position).getQuantity());
                holder.dish_quantity.setText(quantity);
                total += mDishes.get(position).getDish().getPrice();

                String forTotal = "$" + decimalFormat.format(total);
                grand_total.setText(forTotal);

                // Total quantity
                totalQuantity++;
                String number_concat = "Total " + totalQuantity + " items";
                total_number_of_items.setText(number_concat);
            });

            // Decreases the quantity, price, and total items and also changes the object data
            holder.dish_decrease.setOnClickListener(v -> {
                int set = cartItems.get(position).getQuantity();
                if (set != 1) {
                    set--;
                    totalQuantity--;
                    total -= mDishes.get(position).getDish().getPrice();
                }
                cartItems.get(position).setQuantity(set);

                String quantity = Integer.toString(mDishes.get(position).getQuantity());
                holder.dish_quantity.setText(quantity);

                String forTotal = "$" + decimalFormat.format(total);
                grand_total.setText(forTotal);

                String number_concat = "Total " + totalQuantity + " items";
                total_number_of_items.setText(number_concat);
            });

            // Removes the dish from the array
            holder.dish_remove.setOnClickListener(v -> {
                cartItems.remove(position);
                total = 0;
                totalQuantity = 0;
                notifyDataSetChanged();
                if (cartItems.isEmpty()) {
                    grand_total.setText("$0");
                    total_number_of_items.setText("Total 0 items");
                }
            });
        }

        @Override
        public int getItemCount() {
            return mDishes.size();
        }

        private class CartViewHolder extends RecyclerView.ViewHolder {
            TextView dish_name, dish_price, dish_quantity, dish_increase, dish_decrease, dish_remove;
            ImageView dish_art;
            public CartViewHolder(@NonNull View itemView) {
                super(itemView);
                dish_name = itemView.findViewById(R.id.cart_dish_name);
                dish_art = itemView.findViewById(R.id.cart_restaurant_picture);
                dish_price = itemView.findViewById(R.id.cart_price);
                dish_quantity = itemView.findViewById(R.id.cart_quantity);
                dish_increase = itemView.findViewById(R.id.cart_increase);
                dish_decrease = itemView.findViewById(R.id.cart_decrease);
                dish_remove = itemView.findViewById(R.id.cart_remove);
            }
        }
    }
}
