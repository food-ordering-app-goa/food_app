package com.example.foodapp;

import android.app.Dialog;
import android.content.Context;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DishDialogPopUp {

    Context context;
    ImageButton saved_dialog;
    RatingBar dish_rating_set;
    TextView dish_description_dialog, dish_quantity, increase_dialog,
            decrease_dialog, dish_name_dialog, dish_price_dialog, close_dialog;
    ImageView dish_picture_dialog;
    CardView add_to_cart;

    private int quantity, position;
    private String sdFromDB;

    private ArrayList<Dish> mDishes;

    private FirebaseDatabase reference;

    public DishDialogPopUp(Context context, int position, ArrayList<Dish> mDishes) {
        this.context = context;
        this.position = position;
        this.mDishes = mDishes;
    }

    public void initDialog() {

        // Initializing the dialog
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dishpopup);
        quantity = 1;

        // Setting views
        setViewsForDialog(dialog);

        // Changing design of stars
        changeStars(dish_rating_set);

        // Setting description, picture, and name
        dish_description_dialog.setText(context.getResources().getText(R.string.lorem_ipsum));
        dish_picture_dialog.setImageResource(mDishes.get(position).getDrawableId());
        dish_name_dialog.setText(mDishes.get(position).getName());

        // Sets price
        DecimalFormat decimalFormat = new DecimalFormat("###.00");
        String formattedPrice = "$" + decimalFormat.format(mDishes.get(position).getPrice()) + "  -  ";
        dish_price_dialog.setText(formattedPrice);

        // For saved button
        if (ifInSaved(position, mDishes)) {
            saved_dialog.setColorFilter(ContextCompat.getColor(context, R.color.light_grey), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        // Gets saved dish from Firebase
        reference = FirebaseDatabase.getInstance();
        DatabaseReference usersReference = reference.getReference("users");
        Query dataQuery = usersReference.orderByChild("username").equalTo(MainActivity.username);
        dataQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                sdFromDB = snapshot.child(MainActivity.username).child("savedDishes").getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        // Changes the color of the saved button when clicked and adds to the saved array list on SavedFragment
        saved_dialog.setOnClickListener(v -> {
            if (ifInSaved(position, mDishes)) {
                saved_dialog.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
                SavedFragment.savedDishes.remove(mDishes.get(position));
                if (sdFromDB.contains(mDishes.get(position).getName())) {
                    String target = mDishes.get(position).getName() + ";";
                    sdFromDB = sdFromDB.replace(target, "");
                }
            } else {
                saved_dialog.setColorFilter(ContextCompat.getColor(context, R.color.light_grey), android.graphics.PorterDuff.Mode.SRC_IN);
                SavedFragment.savedDishes.add(mDishes.get(position));
                if (!(sdFromDB.contains(mDishes.get(position).getName()))) {
                    sdFromDB = sdFromDB + mDishes.get(position).getName() + ";";
                }
            }
            reference.getReference("users").child(MainActivity.username).child("savedDishes").setValue(sdFromDB);
        });

        add_to_cart.setOnClickListener(v -> {
            ArrayList<CartHelper> tempCartItems = CartFragment.cartItems;
            boolean inCart = false;
            // If dish exists in cart, the quantity is changed.
            for (CartHelper cartHelper : tempCartItems) {
                if (cartHelper.getDish().getName().equals(mDishes.get(position).getName())) {
                    int tempQuantity = cartHelper.getQuantity();
                    tempQuantity += quantity;
                    cartHelper.setQuantity(tempQuantity);
                    inCart = true;
                    break;
                }
            }
            // If dish is not in cart, then the dish is added along with quantity.
            if (!inCart) {
                tempCartItems.add(new CartHelper(mDishes.get(position), quantity));
            }
            FBRating(dish_rating_set, position, mDishes);
            dialog.dismiss();
        });

        // Increases quantity
        increase_dialog.setOnClickListener(v -> {
            quantity++;
            String formattedQuantity = Integer.toString(quantity);
            dish_quantity.setText(formattedQuantity);
        });

        // Decreases quantity
        decrease_dialog.setOnClickListener(v -> {
            if (!(quantity == 1)) {
                quantity--;
            }
            String formattedQuantity = Integer.toString(quantity);
            dish_quantity.setText(formattedQuantity);
        });

        // Closes dialog and records the rating
        close_dialog.setOnClickListener(v -> {
            FBRating(dish_rating_set, position, mDishes);
            dialog.dismiss();
        });

        // Transparent window
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    private String addSavedDishes(String sd, Dish dish) {
        return sd + " " + dish.getName();
    }

    /**
     * Changes the design of the stars
     * @param ratingBar rating bar to change
     */
    public static void changeStars(RatingBar ratingBar) {
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(new BlendModeColorFilter(Color.rgb(255, 255, 0), BlendMode.SRC_ATOP));
        stars.getDrawable(0).setColorFilter(new BlendModeColorFilter(Color.rgb(255, 255, 128), BlendMode.SRC_ATOP));
        stars.getDrawable(1).setColorFilter(new BlendModeColorFilter(Color.rgb(255, 255, 128), BlendMode.SRC_ATOP));
    }

    /**
     * Sets the views
     * @param dialog dialog in which the views are
     */
    private void setViewsForDialog(Dialog dialog) {
        saved_dialog = dialog.findViewById(R.id.saved_dish);
        close_dialog = dialog.findViewById(R.id.exit_out_popup);
        dish_description_dialog = dialog.findViewById(R.id.dish_description_popup);
        dish_quantity = dialog.findViewById(R.id.number_of_items);
        increase_dialog = dialog.findViewById(R.id.increase);
        decrease_dialog = dialog.findViewById(R.id.decrease);
        dish_name_dialog = dialog.findViewById(R.id.dish_popup_name);
        add_to_cart = dialog.findViewById(R.id.add_to_cart);
        dish_picture_dialog = dialog.findViewById(R.id.dish_image_popup);
        dish_rating_set = dialog.findViewById(R.id.dish_rating_set);
        dish_price_dialog = dialog.findViewById(R.id.popup_price);
    }

    /**
     * Checks to see whether the dish is in the savedDishes in SavedFragment
     * @param position position of dish in the data
     * @param mDishes dish array
     * @return whether the dish is in the saved ArrayList
     */
    public static boolean ifInSaved(int position, ArrayList<Dish> mDishes) {
        ArrayList<Dish> tempSavedDish;
        boolean inSaved = false;
        tempSavedDish = SavedFragment.savedDishes;

        for (Dish dish : tempSavedDish) {
            if (dish.getName().equals(mDishes.get(position).getName())) {
                inSaved = true;
                break;
            }
        }

        return inSaved;
    }

    /**
     *
     * @param ratingBar ratingBar
     * @param position position in mDishes
     * @param mDishes ArrayList<Dish> of dishes
     */
    private void FBRating(RatingBar ratingBar, int position, ArrayList<Dish> mDishes) {
        double rating = ratingBar.getRating();
        if (!(rating == 0)) {
            String dishQuery = mDishes.get(position).getName();
            reference = FirebaseDatabase.getInstance();
            double prevRating = mDishes.get(position).getRating();
            double avg = rating;
            if (prevRating != 0) {
                avg = (prevRating + rating) / 2;
            }
            reference.getReference("dishes").child(dishQuery).child("rating").setValue(avg);
        }
    }
}
