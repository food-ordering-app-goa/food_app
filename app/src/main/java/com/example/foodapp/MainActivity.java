package com.example.foodapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNav;

    private String intentUsername;

    public static String username, address;
    public static int fragmentPos = 0;
    public static boolean fromLogin;

    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Checks to see if the intent is from login activity. If so, it gets the username from the intent.
        if (fromLogin)
            initIntent();
        initViews(); // Views assigned
        initFirebase(); // Adds a data listener to each dish and restaurant for ratings.
        initSavedDishes();
        initBottomNav();

    }

    /**
     * Intializes the bundles from the Login screen
     */
    private void initIntent() {
        intentUsername = getIntent().getStringExtra("username");
        username = intentUsername;

        reference = FirebaseDatabase.getInstance().getReference("users");
        Query takingUserData = reference.orderByChild("username").equalTo(intentUsername);

        takingUserData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    address = snapshot.child(intentUsername).child("preferredAddress").getValue(String.class);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    /**
     * Adds dishes on Firebase stored on User into the saved dishes array
     */
    private void initSavedDishes() {
        reference = FirebaseDatabase.getInstance().getReference("users");
        Query dataQuery = reference.orderByChild("username").equalTo(MainActivity.username);
        dataQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    String savedDishesGlued = snapshot.child(intentUsername).child("savedDishes").getValue(String.class);
                    // Find each dish in the data base and place it in array list
                    if (savedDishesGlued != null || savedDishesGlued != "") {
                        String[] arr = savedDishesGlued.split(";");
                        // Shake hands with each one
                        for (String string : arr) {
                            // Shake hands with the restaurants
                            for (Restaurant restaurant : LoginActivity.restaurants) {
                                // Shake hands with the dishes in the restaurants
                                for (Dish meal : restaurant.getDishes()) {
                                    if (meal.getName().equals(string)) {
                                        SavedFragment.savedDishes.add(meal);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    /**
     * Initializes the bottom navigation bar and assigns fragments to each page
     */
    private void initBottomNav() {

        switch (fragmentPos) {
            case 0:
                replaceFragments(new HomeFragment());
                break;
            case 1:
                replaceFragments(new CartFragment());
                break;
            case 2:
                replaceFragments(new SearchFragment());
                break;
        }

        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;

                switch (item.getItemId()) {
                    case R.id.nav_home:
                        selectedFragment = new HomeFragment();
                        break;
                    case R.id.nav_cart:
                        selectedFragment = new CartFragment();
                        break;
                    case R.id.nav_search:
                        selectedFragment = new SearchFragment();
                        break;
                    case R.id.nav_saved:
                        selectedFragment = new SavedFragment();
                        break;
                }

                replaceFragments(selectedFragment);

                return true;
            }
        });
    }

    private void initViews() {
        bottomNav = findViewById(R.id.bottom_navigation);
    }

    /**
     * Replaces the fragments in the fragment_container
     * @param fragment
     */
    public void replaceFragments(Fragment fragment) {
        // Insert the fragment by replacing any existing fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment)
                .commit();
    }

    /**
     * Assigns a data listener to each dish and restaurant for ratings
     */
    private void initFirebase() {
        ArrayList<Restaurant> restaurants = LoginActivity.restaurants;
        reference = FirebaseDatabase.getInstance().getReference("dishes");
        for (int i = 0; i < restaurants.size(); i++) { // Iterates through the dishes in each restaurant
            for (int j = 0; j < restaurants.get(i).getDishes().size(); j++) { // Shakes more hands...
                Query checkName = reference.orderByChild("name").equalTo(restaurants.get(i).getDishes().get(j).getName());
                int finalI = i;
                int finalJ = j;
                checkName.addValueEventListener(new ValueEventListener() {
                    // Sets a listener so that whenever the data is changed in Firebase, it changes the instance value.
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            Double value = snapshot.child(restaurants.get(finalI).getDishes().get(finalJ).getName()).child("rating").getValue(Double.class);
                            restaurants.get(finalI).getDishes().get(finalJ).setRating(value);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        System.out.println("The read failed: " + error.getCode());
                    }
                });
            }
        }
    }
}