package com.example.foodapp;

public class UserProfile {

        String email;
        String password;
        String username;
        String preferredAddress;
        String savedDishes;

        public UserProfile(String email, String password, String username, String preferredAddress, String savedDishes) {
            this.email = email;
            this.password = password;
            this.username = username;
            this.preferredAddress = preferredAddress;
            this.savedDishes = savedDishes;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPreferredAddress() {
            return preferredAddress;
        }

        public void setPreferredAddress(String preferredAddress) {
            this.preferredAddress = preferredAddress;
        }

        public String getSavedDishes() {
            return savedDishes;
        }

        public void setSavedDishes(String savedDishes) {
            this.savedDishes = savedDishes;
        }

        public void addSavedDishes(String string) {
            savedDishes = savedDishes + " " + string;
        }

}