package com.example.foodapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DishesFragment extends Fragment {

    View rootView;

    ImageButton back;
    TextView restaurant_name, restaurant_address, restaurant_phone;

    private int posDishes;
    private int posCuisine;

    ArrayList<Dish> currentDishes = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dishes, container, false);

        initViews();

        back.setOnClickListener(v -> {
            RestaurantFragment restaurantFragment = new RestaurantFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("position", posCuisine);
            restaurantFragment.setArguments(bundle);
            ((MainActivity) getActivity()).replaceFragments(restaurantFragment);
        });

        initRestaurantActivity();
        initRecyclerView();

        return rootView;
    }

    private void initViews() {
        back = rootView.findViewById(R.id.back_button_cuisine_page);
        restaurant_name = rootView.findViewById(R.id.restaurant_name_text);
        restaurant_address = rootView.findViewById(R.id.address);
        restaurant_phone = rootView.findViewById(R.id.phone_number);
    }

    private void initRestaurantActivity() {
        posDishes = getArguments().getInt("position", 0);
        posCuisine = getArguments().getInt("prevPosition", 0);
        currentDishes = LoginActivity.cuisines.get(posCuisine).get(posDishes).getDishes();
        Log.e(" Current Dishes ", " " + currentDishes);
        Log.e(" pos Dishes ", " " + posDishes);
        restaurant_name.setText(LoginActivity.cuisines.get(posCuisine).get(posDishes).getName());
        restaurant_address.setText(LoginActivity.cuisines.get(posCuisine).get(posDishes).getAddress());
        restaurant_phone.setText(LoginActivity.cuisines.get(posCuisine).get(posDishes).getPhoneNo());
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = rootView.findViewById(R.id.restaurant_recycler_view);
        recyclerView.setHasFixedSize(true);
        DishAdapter dishAdapter = new DishAdapter(getContext(), currentDishes);
        recyclerView.setAdapter(dishAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private class DishAdapter extends RecyclerView.Adapter<DishAdapter.DishViewHolder> {

        private Context mContext;
        private ArrayList<Dish> mDishes;

        public DishAdapter(Context mContext, ArrayList<Dish> mDishes) {
            this.mContext = mContext;
            this.mDishes = mDishes;
        }

        @NonNull
        @Override
        public DishViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.dish_items, parent, false);
            return new DishViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull DishViewHolder holder, int position) {
            holder.dish_name.setText(mDishes.get(position).getName());
            Glide.with(mContext).asBitmap().load(mDishes.get(position).getDrawableId()).into(holder.dish_art);
            holder.dish_rating.setRating((float) mDishes.get(position).getRating());
            DecimalFormat decimalFormat = new DecimalFormat("###.00");
            String formattedPrice = "$" + decimalFormat.format(mDishes.get(position).getPrice());
            holder.dish_price.setText(formattedPrice);
            holder.dish_description.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut lacinia tortor.");

            if (DishDialogPopUp.ifInSaved(position, mDishes)) {
                holder.dish_saved.setColorFilter(ContextCompat.getColor(getContext(), R.color.light_grey), android.graphics.PorterDuff.Mode.SRC_IN);
            }

            holder.itemView.setOnClickListener(v -> {
                DishDialogPopUp dishDialogPopUp = new DishDialogPopUp(mContext, position, mDishes);
                dishDialogPopUp.initDialog();
            });
        }

        @Override
        public int getItemCount() {
            return mDishes.size();
        }

        private class DishViewHolder extends RecyclerView.ViewHolder {
            TextView dish_name, dish_description, dish_price;
            ImageView dish_art, dish_saved;
            RatingBar dish_rating;
            public DishViewHolder(@NonNull View itemView) {
                super(itemView);
                dish_name = itemView.findViewById(R.id.dish_name_name);
                dish_art = itemView.findViewById(R.id.dish_picture);
                dish_description = itemView.findViewById(R.id.dish_description);
                dish_rating = itemView.findViewById(R.id.dish_rating_bar);
                dish_price = itemView.findViewById(R.id.price);
                dish_saved = itemView.findViewById(R.id.saved_indicator);
                DishDialogPopUp.changeStars(dish_rating);
            }
        }
    }
}
