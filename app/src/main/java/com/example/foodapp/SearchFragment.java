package com.example.foodapp;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class SearchFragment extends Fragment {

    View rootView;
    EditText searchView;
    RecyclerView recyclerView;
    SearchAdapter searchAdapter;

    private ArrayList<Dish> dishSearch = new ArrayList<>();
    private ArrayList<Restaurant> tempRestaurants = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search, container, false);

        initViews();
        initDishSearch();
        initRecyclerView();
        initSearchView();

        return rootView;
    }

    private void initSearchView() {
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initViews() {
        searchView = rootView.findViewById(R.id.search_dishes);
        recyclerView = rootView.findViewById(R.id.search_recycler_view);
    }

    private void initRecyclerView() {
        searchAdapter = new SearchAdapter(getContext(), dishSearch);
        recyclerView.setAdapter(searchAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }

    private void initDishSearch() {
        tempRestaurants = LoginActivity.restaurants;
        // Takes all the dishes and puts it into one array list.
        for (int i = 0; i < tempRestaurants.size(); i++) {
            dishSearch.addAll(tempRestaurants.get(i).getDishes());
        }
    }

    private static class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> implements Filterable {

        private Context mContext;
        private ArrayList<Dish> mDishes;
        private ArrayList<Dish> mDishesFull;

        public SearchAdapter(Context mContext, ArrayList<Dish> mDishes) {
            this.mContext = mContext;
            this.mDishes = mDishes;
            mDishesFull = new ArrayList<>(mDishes);
        }

        @NonNull
        @Override
        public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.dish_items, parent, false);
            return new SearchViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
            holder.dish_name.setText(mDishes.get(position).getName());
            Glide.with(mContext).asBitmap().load(mDishes.get(position).getDrawableId()).into(holder.dish_art);
            holder.dish_rating.setRating((float) mDishes.get(position).getRating());
            DecimalFormat decimalFormat = new DecimalFormat("###.##");
            String formattedPrice = "$" + decimalFormat.format(mDishes.get(position).getPrice());
            holder.dish_price.setText(formattedPrice);
            holder.dish_description.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut lacinia tortor.");
            holder.itemView.setOnClickListener(v -> {
                DishDialogPopUp dishDialogPopUp = new DishDialogPopUp(mContext, position, mDishes);
                dishDialogPopUp.initDialog();
            });
        }

        @Override
        public int getItemCount() {
            return mDishes.size();
        }

        public static final class SearchViewHolder extends RecyclerView.ViewHolder {
            TextView dish_name, dish_description, dish_price;
            ImageView dish_art;
            RatingBar dish_rating;
            public SearchViewHolder(@NonNull View itemView) {
                super(itemView);
                dish_name = itemView.findViewById(R.id.dish_name_name);
                dish_art = itemView.findViewById(R.id.dish_picture);
                dish_description = itemView.findViewById(R.id.dish_description);
                dish_rating = itemView.findViewById(R.id.dish_rating_bar);
                dish_price = itemView.findViewById(R.id.price);
                DishDialogPopUp.changeStars(dish_rating);
            }
        }

        /**
         * Every time the user enters something, it filters the recycler view
         * @return the Filter
         */
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    // filters through the recycler view and returns the filtered list
                    ArrayList<Dish> filteredList = new ArrayList<>();

                    if (constraint == null || constraint.length() == 0) {
                        filteredList.addAll(mDishesFull);
                    } else {
                        String key = constraint.toString().toLowerCase().trim();
                        for (Dish dish : mDishesFull) {
                            if (dish.getName().toLowerCase().contains(key) || dish.getRestaurant().toLowerCase().contains(key) || Double.toString(dish.getPrice()).contains(key)) {
                                filteredList.add(dish);
                            }
                        }
                    }


                    FilterResults filterResults = new FilterResults();
                    filterResults.values = filteredList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {

                    mDishes.clear();
                    mDishes.addAll((ArrayList<Dish>)results.values);
                    notifyDataSetChanged();

                }
            };
        }
    }
}
