# Edible

Edible is an efficient and clean food delivery app.

## Description

Our project lists food in a clean and organized way. In addition, it provides helpful tools for an efficient experience, such as ratings, search option, and saved dishes on a cloud database.

### Features

* Rating System
* Usage of Cloud Database for Rating and User Storage
* Saved Dishes Directly to your Account
* Google Maps to find Address
* Progress Bar
* Search Feature

## Authors and Acknowledgements

Developers - Mackenzie Dy, Shane So, Tamara Nammao, Mason 

Designers - Adrian Lo, Josh Young